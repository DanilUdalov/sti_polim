class Owner < ActiveRecord::Base
  belongs_to :owneable, polymorphic: true
end
