class Animal < ActiveRecord::Base
  has_many :owners, as: :owneable
end
