Rails.application.routes.draw do
  get 'pets/index'
  get 'cats/index'
  get 'dogs/index'
  get 'pets/show'
  get 'pets/new'
  get 'pets/edit'
  root 'pets#index'

  resources :pets
  resources :dogs
  resources :cats

end