class CreateOwners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.references :owneable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end
